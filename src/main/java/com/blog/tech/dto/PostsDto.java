package com.blog.tech.dto;

import java.util.Date;

import com.blog.tech.model.TblBlogs;
import com.blog.tech.model.TblUser;

public class PostsDto {

	private int id;

	private String name;

	private TblBlogs tblBlogs;

	private String postContent;

	private int authorId;
	
	private TblUser author;

	private Integer parentId;

	private int blogId;

	private int isActive;

	private Date dateCreated;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public TblBlogs getTblBlogs() {
		return tblBlogs;
	}

	public void setTblBlogs(TblBlogs tblBlogs) {
		this.tblBlogs = tblBlogs;
	}

	public String getPostContent() {
		return postContent;
	}

	public void setPostContent(String postContent) {
		this.postContent = postContent;
	}

	public int getAuthorId() {
		return authorId;
	}

	public void setAuthorId(int authorId) {
		this.authorId = authorId;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public int getBlogId() {
		return blogId;
	}

	public void setBlogId(int blogId) {
		this.blogId = blogId;
	}

	public int getIsActive() {
		return isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public TblUser getAuthor() {
		return author;
	}

	public void setAuthor(TblUser author) {
		this.author = author;
	}
}
