package com.blog.tech.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "categories")
public class TblCategories {
    @Id
	@SequenceGenerator(name = "CATEGORIES_ID_GENERATOR", sequenceName = "CATEGORIES_SEQ", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CATEGORIES_ID_GENERATOR")
    @Column(name="id")
    private int id;
    
    @Column(name="category_name")
    private String categoryName;
    
    @OneToMany(mappedBy = "tblCategories")
    private List<TblBlogs> blogs;
    
	public List<TblBlogs> getBlogs() {
		return blogs;
	}
	public void setBlogs(List<TblBlogs> blogs) {
		this.blogs = blogs;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
}
