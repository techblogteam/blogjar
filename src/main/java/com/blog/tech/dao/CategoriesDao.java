package com.blog.tech.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.blog.tech.model.TblCategories;

public interface CategoriesDao extends JpaRepository<TblCategories, Integer>  {
	
}
