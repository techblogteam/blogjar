package com.blog.tech.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.blog.tech.model.TblPost;

public interface PostsDao extends JpaRepository<TblPost, Integer>{

}
