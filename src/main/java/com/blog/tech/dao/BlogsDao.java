package com.blog.tech.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.blog.tech.model.TblBlogs;

public interface BlogsDao extends JpaRepository<TblBlogs, Integer>{

}
