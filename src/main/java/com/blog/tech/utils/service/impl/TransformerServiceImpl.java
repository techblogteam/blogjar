package com.blog.tech.utils.service.impl;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;

import com.blog.tech.dto.PostsDto;
import com.blog.tech.model.TblPost;
import com.blog.tech.utils.service.TransformerService;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class TransformerServiceImpl implements TransformerService{

	Mapper mapper = new DozerBeanMapper();;

	ObjectMapper objectMapper = new ObjectMapper();	
	
	@Override
	public PostsDto toPostsDto(TblPost tblPosts) {
		return mapper.map(tblPosts, PostsDto.class);
	}

}
