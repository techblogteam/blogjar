package com.blog.tech.utils.service;

import java.util.List;

import com.blog.tech.dto.PostsDto;
import com.blog.tech.model.TblPost;

public interface TransformerService {
	
	PostsDto toPostsDto(TblPost tblPosts);
}
