package com.blog.tech.service;

import com.blog.tech.dto.ApiResponse;
import com.blog.tech.dto.PostsDto;
import com.blog.tech.model.TblPost;

public interface PostsService {
	
	ApiResponse<PostsDto> getAllPosts(String token);
}
