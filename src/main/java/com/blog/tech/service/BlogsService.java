package com.blog.tech.service;

import java.util.List;

import com.blog.tech.dto.ApiResponse;
import com.blog.tech.model.TblBlogs;

public interface BlogsService {
	
	ApiResponse<List<TblBlogs>> getAllBlogs(String token);

}
