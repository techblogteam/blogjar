package com.blog.tech.service;

import com.blog.tech.dto.ApiResponse;
import com.blog.tech.dto.AuthToken;
import com.blog.tech.dto.LoginUser;
import com.blog.tech.dto.UserDto;
import com.blog.tech.model.TblUser;

import java.util.List;

import org.springframework.security.authentication.AuthenticationManager;

public interface UserService {

	ApiResponse<AuthToken> login(LoginUser loginUser, String token, AuthenticationManager authenticationManager);

	TblUser save(UserDto user);

	List<TblUser> findAll();

	void delete(int id);

	TblUser findOne(String username);

	TblUser findById(int id);

	UserDto update(UserDto userDto);
}
