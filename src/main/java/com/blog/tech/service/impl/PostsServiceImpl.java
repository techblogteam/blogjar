package com.blog.tech.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.blog.tech.dao.PostsDao;
import com.blog.tech.dto.ApiResponse;
import com.blog.tech.dto.PostsDto;
import com.blog.tech.model.TblCategories;
import com.blog.tech.model.TblPost;
import com.blog.tech.model.TblTokens;
import com.blog.tech.service.PostsService;
import com.blog.tech.service.TokenService;
import com.blog.tech.service.UserService;
import com.blog.tech.utils.service.TransformerService;

@Service
public class PostsServiceImpl implements PostsService {

	@Autowired
	private PostsDao postsDao;
	
	@Autowired
	private TokenService tokenService;
	
	@Autowired
	private TransformerService transformerService;
	
	@Autowired
	private UserService userService;

	@Override
	public ApiResponse<PostsDto> getAllPosts(String token) {
		TblTokens tokenObject = tokenService.findTokenByName(token.replaceFirst("Bearer ", ""));
		if (tokenObject == null || tokenObject.getToken() == "") {
			return new ApiResponse<PostsDto>(HttpStatus.FORBIDDEN.value(), "Not Allowed",
					new ArrayList<TblCategories>());
		}
		List<PostsDto> result = new ArrayList<PostsDto>();
		postsDao.findAll().forEach(tblPost->{
			PostsDto postDto = transformerService.toPostsDto(tblPost);
			postDto.setAuthor(userService.findById(postDto.getAuthorId()));
			result.add(postDto);
		});
		return new ApiResponse<PostsDto>(HttpStatus.OK.value(), "Success", result);
	}

}
