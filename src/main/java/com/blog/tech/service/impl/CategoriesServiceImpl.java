package com.blog.tech.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.blog.tech.dao.CategoriesDao;
import com.blog.tech.dto.ApiResponse;
import com.blog.tech.model.TblCategories;
import com.blog.tech.model.TblTokens;
import com.blog.tech.service.CategoriesService;
import com.blog.tech.service.TokenService;

@Service
public class CategoriesServiceImpl implements CategoriesService {

	@Autowired
	CategoriesDao categoriesDao;

	@Autowired
	private TokenService tokenService;

	@Override
	public ApiResponse<List<TblCategories>> getAllCategories(String token) {
		TblTokens tokenObject = tokenService.findTokenByName(token.replaceFirst("Bearer ", ""));
		if (tokenObject == null || tokenObject.getToken() == "") {
			return new ApiResponse<List<TblCategories>>(HttpStatus.FORBIDDEN.value(), "Not Allowed",
					new ArrayList<TblCategories>());
		}
		return new ApiResponse<List<TblCategories>>(HttpStatus.OK.value(), "Success", categoriesDao.findAll());
	}

	@Override
	public ApiResponse<TblCategories> saveCategory(TblCategories category, String token) {
		TblTokens tokenObject = tokenService.findTokenByName(token.replaceFirst("Bearer ", ""));
		if (tokenObject == null || tokenObject.getToken() == "") {
			return new ApiResponse<TblCategories>(HttpStatus.FORBIDDEN.value(), "Not Allowed",
					new ArrayList<TblCategories>());
		}
		TblCategories savedCategory = new TblCategories();
		savedCategory = categoriesDao.save(category);

		return new ApiResponse<TblCategories>(HttpStatus.OK.value(), "Success", savedCategory);
	}

	@Override
	public ApiResponse<List<TblCategories>> saveAllCategories(List<TblCategories> categoriesList, String token) {
		TblTokens tokenObject = tokenService.findTokenByName(token.replaceFirst("Bearer ", ""));
		if (tokenObject == null || tokenObject.getToken() == "") {
			return new ApiResponse<List<TblCategories>>(HttpStatus.FORBIDDEN.value(), "Not Allowed",
					new ArrayList<TblCategories>());
		}
		List<TblCategories> listCategories = new ArrayList<TblCategories>();
		listCategories = categoriesDao.saveAll(categoriesList);
		return new ApiResponse<List<TblCategories>>(HttpStatus.OK.value(), "Success", listCategories);
	}

}
