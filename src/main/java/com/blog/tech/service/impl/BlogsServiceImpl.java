package com.blog.tech.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.blog.tech.dao.BlogsDao;
import com.blog.tech.dto.ApiResponse;
import com.blog.tech.model.TblBlogs;
import com.blog.tech.model.TblCategories;
import com.blog.tech.model.TblTokens;
import com.blog.tech.service.BlogsService;
import com.blog.tech.service.TokenService;

@Service
public class BlogsServiceImpl implements BlogsService{

	@Autowired
	private BlogsDao blogsDao;
	
	@Autowired
	private TokenService tokenService;
	
	
	@Override
	public ApiResponse<List<TblBlogs>> getAllBlogs(String token) {
		TblTokens tokenObject = tokenService.findTokenByName(token.replaceFirst("Bearer ", ""));
		if (tokenObject == null || tokenObject.getToken() == "") {
			return new ApiResponse<List<TblBlogs>>(HttpStatus.FORBIDDEN.value(), "Not Allowed", new ArrayList<TblCategories>());
		}
		List<TblBlogs> blogsList = blogsDao.findAll();
		return new ApiResponse<>(HttpStatus.OK.value(), "Success", blogsList);
	}
}
