package com.blog.tech.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.blog.tech.dao.UserDao;
import com.blog.tech.dto.ApiResponse;
import com.blog.tech.dto.AuthToken;
import com.blog.tech.dto.LoginUser;
import com.blog.tech.dto.TokenDto;
import com.blog.tech.dto.UserDto;
import com.blog.tech.model.TblUser;
import com.blog.tech.service.TokenService;
import com.blog.tech.service.UserService;

@Component
@Service("userService")
public class UserServiceImpl implements UserDetailsService, UserService {

	@Autowired
	private UserDao userDao;

	@Autowired
	private TokenService tokenService;

	@Autowired()
	private BCryptPasswordEncoder bcryptEncoder;

	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		TblUser user = userDao.findByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException("Invalid username or password.");
		}
		return new User(user.getUsername(), user.getPassword(), getAuthority());
	}

	private List<SimpleGrantedAuthority> getAuthority() {
		return Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"));
	}

	public List<TblUser> findAll() {
		List<TblUser> list = new ArrayList<>();
		userDao.findAll().iterator().forEachRemaining(list::add);
		return list;
	}

	@Override
	public void delete(int id) {
		userDao.deleteById(id);
	}

	@Override
	public TblUser findOne(String username) {
		return userDao.findByUsername(username);
	}

	@Override
	public TblUser findById(int id) {
		TblUser user = userDao.findById(id);
		return user;
	}

	@Override
	public UserDto update(UserDto userDto) {
		TblUser user = findById(userDto.getId());
		if (user != null) {
			BeanUtils.copyProperties(userDto, user, "password");
			userDao.save(user);
		}
		return userDto;
	}

	@Override
	public TblUser save(UserDto user) {
		TblUser newUser = new TblUser();
		newUser.setUsername(user.getUsername());
		newUser.setFirstName(user.getFirstName());
		newUser.setLastName(user.getLastName());
		newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
		newUser.setAge(user.getAge());
		newUser.setSalary(user.getSalary());
		return userDao.save(newUser);
	}

	@Override
	public ApiResponse<AuthToken> login(LoginUser loginUser, String token,
			AuthenticationManager authenticationManager) {
		final TblUser user = this.findOne(loginUser.getUsername());
		TokenDto tokenObject = new TokenDto();
		tokenObject.setToken(token);
		if (user == null) {
			return new ApiResponse<AuthToken>(HttpStatus.FORBIDDEN.value(), "Username not found", new AuthToken());
		} else if (!bcryptEncoder.matches(loginUser.getPassword(), user.getPassword())) {
			return new ApiResponse<AuthToken>(HttpStatus.FORBIDDEN.value(), "Incorrect Password", new AuthToken());
		}
		return new ApiResponse<>(HttpStatus.OK.value(), tokenService.inserTokens(tokenObject, user.getId()),
				new AuthToken(token, loginUser.getUsername()));
	}
}
