package com.blog.tech.service;

import java.util.List;

import com.blog.tech.dto.ApiResponse;
import com.blog.tech.model.TblCategories;

public interface CategoriesService {
	ApiResponse<List<TblCategories>> getAllCategories(String token);
	
	ApiResponse<TblCategories> saveCategory(TblCategories category, String token);
	
	ApiResponse<List<TblCategories>> saveAllCategories(List<TblCategories> categoriesList, String token);
}
